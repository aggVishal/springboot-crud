package com.crud.example.crud.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
public class ProductRequest {

    @NotBlank(message = "name can't be null or empty")
    private String name;
    @Min(1)
    private int quantity;
    @Min(10)
    private double prize;
}
