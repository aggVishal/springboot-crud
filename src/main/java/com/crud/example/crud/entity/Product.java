package com.crud.example.crud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT_TBL")
@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@Data
public class Product {
    @Id
    @GeneratedValue
    private int id;

    private String name;
    private int quantity;
    private double prize;
}
