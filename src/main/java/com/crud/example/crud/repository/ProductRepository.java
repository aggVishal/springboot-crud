package com.crud.example.crud.repository;

import com.crud.example.crud.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Integer> {
    public Product findByName(String name);
}