package com.crud.example.crud.controller;
import com.crud.example.crud.dto.ProductRequest;
import com.crud.example.crud.entity.Product;
import com.crud.example.crud.exception.UserNotFoundException;
import com.crud.example.crud.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ProductController {

    @Autowired
    public ProductService service;

    @GetMapping("/products")
    public ResponseEntity<List<Product>> getProducts(){
        return ResponseEntity.ok(service.getProducts());
    }

    @GetMapping("/productById/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable int id) throws UserNotFoundException {
        return ResponseEntity.ok(service.getProductById(id));
    }

    @GetMapping("/productByName/{name}")
    public ResponseEntity<Product> getProductByName(@PathVariable String name) throws UserNotFoundException {
        return ResponseEntity.ok(service.getProductByName(name));
    }

    @PostMapping("/product")
    public ResponseEntity<Product> putProduct(@RequestBody @Valid ProductRequest productRequest){
        return new ResponseEntity(service.saveProduct(productRequest),HttpStatus.CREATED);
    }

    @PostMapping("/products")
    public ResponseEntity<List<Product>> putProduct(@RequestBody List<Product> products){
        return new ResponseEntity(service.saveProducts(products),HttpStatus.CREATED);
    }

    @PutMapping("/updateProduct/{id}")
    public ResponseEntity<Product> updateProduct(@RequestBody Product product, @PathVariable int id) throws UserNotFoundException {
        return ResponseEntity.ok(service.updateProduct(product,id));
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable int id) throws UserNotFoundException {
        return ResponseEntity.ok(service.deleteProduct(id));
    }
}
