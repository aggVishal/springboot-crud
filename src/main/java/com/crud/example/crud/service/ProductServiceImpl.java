package com.crud.example.crud.service;

import com.crud.example.crud.dto.ProductRequest;
import com.crud.example.crud.entity.Product;
import com.crud.example.crud.exception.UserNotFoundException;
import com.crud.example.crud.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository repository;

    @Override
    public Product saveProduct(ProductRequest productRequest) {
        Product p = Product.build(0, productRequest.getName(), productRequest.getQuantity(), productRequest.getPrize());
        return repository.save(p);
    }

    @Override
    public List<Product> saveProducts(List<Product> products) {
        return repository.saveAll(products);
    }

    @Override
    public List<Product> getProducts() {
        return repository.findAll();
    }

    @Override
    public Product getProductById(int id) throws UserNotFoundException {
        Product p = repository.findById(id).orElse(null);
        if(p != null){
            return p;
        }
        else{
            throw new UserNotFoundException("user not found with Id: "+id);
        }
    }

    @Override
    public Product getProductByName(String name) throws UserNotFoundException {
        Product p = repository.findByName(name);
        if(p != null){
            return p;
        }
        else{
            throw new UserNotFoundException("user not found with name: "+name);
        }
    }

    @Override
    public String deleteProduct(Integer id) throws UserNotFoundException {
        Product p = repository.findById(id).orElse(null);
        if(p!=null){
            repository.deleteById(id);
            return "Product (" + id + ") deleted successfully!";
        }
        else{
            throw new UserNotFoundException("user not found with id: "+id);
        }

    }

    @Override
    public Product updateProduct(Product product, int id) throws UserNotFoundException {
        Product existingProduct = repository.findById(id).orElse(null);
        if(existingProduct!=null){
            existingProduct.setName(product.getName());
            existingProduct.setPrize(product.getPrize());
            existingProduct.setQuantity(product.getQuantity());
            return repository.save(existingProduct);
        }
        else{
            throw new UserNotFoundException("user not found with id: "+id);
        }
    }
}