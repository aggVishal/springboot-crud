package com.crud.example.crud.service;

import com.crud.example.crud.dto.ProductRequest;
import com.crud.example.crud.entity.Product;
import com.crud.example.crud.exception.UserNotFoundException;

import java.util.List;

public interface ProductService {
    public Product saveProduct(ProductRequest productRequest);

    public List<Product> saveProducts(List<Product> products);

    public List<Product> getProducts();

    public Product getProductById(int id) throws UserNotFoundException;

    public Product getProductByName(String name) throws UserNotFoundException;

    public String deleteProduct(Integer id) throws UserNotFoundException;

    public Product updateProduct(Product product, int id) throws UserNotFoundException;
}
